package org.elu.kotlin.learning.classes

enum class PrioritySimple {
    MINOR, NORMAL, MAJOR, CRITICAL
}

enum class Priority(val value: Int) {
    MINOR(-1),
    NORMAL(0),
    MAJOR(1),
    CRITICAL(10)
}

enum class PriorityComplex(val value: Int) {
    MINOR(-1) {
        override fun text(): String {
            return "[MINOR PRIORITY]"
        }

        override fun toString(): String {
            return "Minor Priority"
        }
    },
    NORMAL(0) {
        override fun text(): String {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
    },
    MAJOR(1) {
        override fun text(): String {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
    },
    CRITICAL(10) {
        override fun text(): String {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
    };

    abstract fun text(): String
}

fun main(args: Array<String>) {
    val prioritySimple = PrioritySimple.NORMAL
    println(prioritySimple)

    val priority = Priority.NORMAL
    println(priority)
    println(priority.value)
    println(priority.ordinal)
    println()

    println(Priority.CRITICAL)
    println(Priority.CRITICAL.value)
    println(Priority.CRITICAL.ordinal)
    println(Priority.CRITICAL.name)
    println()

    for (priorityInList in Priority.values()) {
        println(priorityInList)
    }
    println()

    val p = Priority.valueOf("MAJOR")
    println(p)
    println(p.ordinal)
    println()

    val complex = PriorityComplex.MINOR
    println(complex)
    println(complex.text())
}
