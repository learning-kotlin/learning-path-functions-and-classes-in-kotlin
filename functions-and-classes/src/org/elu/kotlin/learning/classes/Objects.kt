package org.elu.kotlin.learning.classes

object Global {
    val PI = 3.14
}

fun main(args: Array<String>) {
    println(Global.PI)

    val localObject = object {
        val PI = 3.14159
    }
    println(localObject.PI)
}
