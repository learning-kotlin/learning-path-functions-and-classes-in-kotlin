package org.elu.kotlin.learning.classes

import java.util.*

class Customer {
    var id = 0
    var name: String = ""
}

class Customer1(val id: Int, var name: String = "")

class Customer2(val id: Int, var name: String) {
    init {
        name = name.toUpperCase()
    }

    constructor(email: String): this(0, "") {
        // some code here
    }
}

class SuperCustomer(val id: Int, var name: String, val yearOfBirth: Int) {
    val age: Int
        get() = Calendar.getInstance().get(Calendar.YEAR) - yearOfBirth

    var socialSecurityNumber: String = ""
        set(value) {
            if (!value.startsWith("SN")) {
                throw IllegalArgumentException("Social security should start with SN")
            }
            field = value
        }

    fun customerAsString(): String {
        return "Id: $id - Name: $name - Age: $age"
    }
}

fun main(args: Array<String>) {
    val customer = Customer()

    customer.id = 5
    customer.name = "Edu"
    println("${customer.id} ${customer.name}")

    val customer1 = Customer1(10, "Janna")
    println("${customer1.id} ${customer1.name}")

    val customer2 = Customer1(1)
    println("${customer2.id} ${customer2.name}")

    val customer3 = Customer2(6, "Arno")
    println("${customer3.id} ${customer3.name}")

    var customer4 = SuperCustomer(5, "Edu", 1968)
    println("${customer4.id} ${customer4.name} ${customer4.age}")

    customer4.socialSecurityNumber = "SN1234"
    println(customer4.socialSecurityNumber)

    println(customer4.customerAsString())
}