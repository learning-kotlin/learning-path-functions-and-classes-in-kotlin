package org.elu.kotlin.learning.classes

data class CustomerKotlin(var id: Int, var name: String, var email: String)

data class CustomerKotlin2(var id: Int, var name: String, var email: String) {
    override fun toString(): String {
        return "{\"id\": \"$id\", \"name\": \"$name\"}"
    }
}

fun main(args: Array<String>) {
    val customer1 = CustomerKotlin(1, "Edu", "edu@mail.com")
    val customer2 = CustomerKotlin(1, "Edu", "edu@mail.com")

    println(customer1)

    if (customer1 == customer2) {
        println("They are the same")
    }

    val customer3 = customer1
    println(customer3)
    val customer4 = customer1.copy(email = "edu@gmail.com")
    println(customer4)

    val customer5 = CustomerKotlin2(2, "Janna", "janna@mail.com")
    println(customer5)
}
