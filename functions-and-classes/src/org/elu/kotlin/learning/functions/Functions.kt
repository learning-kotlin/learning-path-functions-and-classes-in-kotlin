package org.elu.kotlin.learning.functions

fun hello(): Unit { // Unit means function returns no value
    println("Hello")
}

fun throwingException(): Nothing { // Nothing means that function never returns
    throw Exception("This function throws an exception")
}

fun returnsFour(): Int {
    return 4
}

fun takingString(name: String) {
    println(name)
}

fun sum(x: Int, y: Int) = x + y

// Default parameters
fun sum3(x: Int, y: Int, z: Int = 0) = x + y + z

fun printDetails(name: String, email: String = "", phone: String = "NA") {
    println("name: $name - email: $email - phone: $phone")
}

// variadic functions
fun printStrings(vararg strings: String) {
    reallyPrintStrings(*strings) // spread operator
}

private fun reallyPrintStrings(vararg strings: String) {
    for (string in strings) {
        println(string)
    }
}

fun main(args: Array<String>) {
    hello()

    val value = returnsFour()
    println("1) " + value)

    takingString("2) Some value")

    println("3) sum: " + sum(2, 3))

    println("4) sum3: " + sum3(2, 3, 4))
    println("5) sum3: " + sum3(2, 3))

    // usage of named parameters
    printDetails("Edu", phone = "079 12345678")
    printDetails("Edu", phone = "079 12345678", email = "edu@mail.com")
    printDetails("Edu")

    printStrings("1")
    printStrings("1", "2")
    printStrings("1", "2", "3")
    printStrings("1", "2", "3", "4")
}
